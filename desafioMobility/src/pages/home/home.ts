import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DesafioMobilityProvider } from '../../providers/desafio-mobility/desafio-mobility';
import { CartPage}  from '../../pages/cart/cart';
import { ProductPage } from '../../pages/product/product';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {
  
  products: Array<{title: string, image: string, size: number, sugar: number, additional: Array<{item: string}>[]}>;
  
  constructor(public navCtrl: NavController, public desafioMobilityService: DesafioMobilityProvider) {
	
	desafioMobilityService.getRemoteData();	
	this.products = desafioMobilityService.products;
  
  }
  
  openProduct(p) {
    //Abre a p�gina de detalhes do Produto
	this.navCtrl.push(ProductPage, { p } );
  }
  
  openCart() {
    //Abre a p�gina do carrinho
	this.navCtrl.setRoot(CartPage);
  }

}
