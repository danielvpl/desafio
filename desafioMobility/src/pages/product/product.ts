import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular';
import { CartPage } from '../../pages/cart/cart';
import { DesafioMobilityProvider } from '../../providers/desafio-mobility/desafio-mobility';

@Component({
  templateUrl: 'product.html'
})
	
export class ProductPage {
	
	selectedItem: any;
	title: string;
	image: string;
	size: number;	
	sugar: number;
	quant: number;
	adds: any = [];
	additional: Array<{item: string}>;
	cupsize: string = "";
	
	constructor(public navCtrl: NavController, public navParams: NavParams, public desafioMobilityService: DesafioMobilityProvider) {
		this.selectedItem = navParams.get('p');
		this.title = this.selectedItem.title;
		this.image = this.selectedItem.image;
		this.size = this.selectedItem.size;
		this.sugar = this.selectedItem.sugar;		
		this.cupsize = this.desafioMobilityService.getCupSize(this.size)+"";
		this.additional = this.selectedItem.additional;						
		this.quant = 0;		
		//this.adds = [];
	}
  
	Increment () {
	  if(this.quant <= 50) //M�ximo 50
		this.quant++;	  
	}

	Decrement () {
	  if(this.quant > 0) //M�nimo 0
		this.quant--;
	}
	
	ChangeSize (value) {
	  this.size = value;
	  this.cupsize = this.desafioMobilityService.getCupSize(this.size)+"";		
	}
	
	SelectAdditional (valor) {	  
	  let val:string = valor;
	  if(this.adds.indexOf(valor) != -1){	    
		this.adds.pop(val);
	  }
	  else{
		this.adds.push(val);
	  }
	}
	
	orderSubmit( item ) {
		//console.log(item.adds);
	    if(item.quant > 0){
			CartPage.addProduct( item );
			this.adds = [];
			//Navegabilidade em pilhas
			this.navCtrl.setRoot(CartPage, { item } );		
		}
	}
  
}	