import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
import { DesafioMobilityProvider } from '../../providers/desafio-mobility/desafio-mobility';

@Component({
  selector: 'page-list',
  templateUrl: 'cart.html'
})

export class CartPage {
  selectedItem: any;
  //Utilizei vari�vel est�tica para evitar a necessidade de salvar os valores
  static items: Array<{id:number, title: string, quant: number, adds: string, size: number}>;
  cartItens: Array<{id:number, title: string, quant: number, adds: string, size: number}>;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public desafioMobilityService: DesafioMobilityProvider) {
	//this.selectedItem = navParams.get('item');  
	this.cartItens = CartPage.items;
  }
  
  public static addProduct(item){	
	CartPage.items = CartPage.items == null ? [] : CartPage.items;   	
	  CartPage.items.push({
		id: CartPage.items.length + 1,
		title: item.title,
		quant: item.quant,
		adds: item.adds,
		size: item.size
	  });
  }
  
  public clearCart(){
	CartPage.items = [];
	this.navCtrl.setRoot(CartPage);	
  }
  
  returnHome() {
    //Abre a p�gina do carrinho
	this.navCtrl.setRoot(HomePage);	
  }
  
}
