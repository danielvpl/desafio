import { Component } from '@angular/core';
import { ViewController } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { NavController } from 'ionic-angular';
import { HomePage } from '../../pages/home/home';
 
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {
 
  constructor(public viewCtrl: ViewController, public splashScreen: SplashScreen, public navCtrl: NavController) {
 
  }
 
  ionViewDidEnter() {
 
    this.splashScreen.hide();
 
    setTimeout(() => {
      this.viewCtrl.dismiss();
    }, 1000);
	
	this.navCtrl.setRoot(HomePage);		
 
  }
 
}